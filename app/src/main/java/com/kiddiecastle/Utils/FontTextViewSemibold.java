package com.kiddiecastle.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by harini on 6/12/2018.
 */

public class FontTextViewSemibold extends TextView {
    public FontTextViewSemibold(Context context) {
        super(context);
        setIncludeFontPadding(false);
        setTypeface(HelveticaFont.getInstance(context).getSemiBoldTypeFace());
    }

    public FontTextViewSemibold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setIncludeFontPadding(false);
        setTypeface(HelveticaFont.getInstance(context).getSemiBoldTypeFace());
    }

    public FontTextViewSemibold(Context context, AttributeSet attrs,
                             int defStyle) {
        super(context, attrs, defStyle);
        setIncludeFontPadding(false);
        setTypeface(HelveticaFont.getInstance(context).getSemiBoldTypeFace());
    }
}
