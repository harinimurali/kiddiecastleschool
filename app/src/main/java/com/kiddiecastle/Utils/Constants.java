package com.kiddiecastle.Utils;

/**
 * Created by harini on 6/13/2018.
 */

public class Constants {
    public static final String NUMBER = "PhoneNumber";
    public static final String SCHOOLID = "schoolid";
    public static final String INTENT_COUNTRY_CODE = "code";



    //Appreferences data
    public static final String FATHERNAME = "fathername";
    public static final String PROFILEIMAGE = "profileimage";
    public static final String CHILDREN_PROFILE = "children_profile";
    public static final String LANGUAGE_CHANGE = "change";
    public static final String LANGUAGE = "language";
    public static final String FCMTOKEN = "fcmtoken";
    public static final String SCHOOL_NAME = "schoolname";
    public static final String VERSION_CODE = "versioncode";

}
