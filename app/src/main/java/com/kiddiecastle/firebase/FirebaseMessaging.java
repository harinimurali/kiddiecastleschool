package com.kiddiecastle.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kiddiecastle.Activity.MainActivity;
import com.kiddiecastle.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by vijayaganesh on 11/13/2017.
 */

public class FirebaseMessaging extends FirebaseMessagingService {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String TAG = "FirebaseMessagings";
    Bitmap bitmap;
    String message;
    String type = "";
    String imageUri = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //Calling method to generate icon_notification
        Log.d("FirebaseMessagings", "onMessageReceived: " + remoteMessage.getNotification());
        //sendNotification(remoteMessage.getNotification());

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        //The message which i send will have keys named [message, image, AnotherActivity] and corresponding values.
        //You can change as per the requirement.

        //message will contain the Push Message
        if (remoteMessage.getData().get("message") != null) {
            try {
                JSONObject jsonObject = new JSONObject(remoteMessage.getData().get("message"));
                if (jsonObject.has("content")) {
                    message = jsonObject.getString("content");
                }/*else if(jsonObject.has("activityName")){
                    message = jsonObject.getString("activityName");
                }*/

                type = jsonObject.getString("notifType");
                Log.e("type", "" + type);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //message = remoteMessage.getData().get("message");
        }
        if (remoteMessage.getData().get("image") != null) {
            imageUri = remoteMessage.getData().get("image");
        }
        if (remoteMessage.getData().get("message") != null) {

        }

        //imageUri will contain URL of the image to be displayed with Notification

        //If the key AnotherActivity has  value as True then when the user taps on notification, in the app AnotherActivity will be opened.
        //If the key AnotherActivity has  value as False then when the user taps on notification, in the app MainActivity will be opened.
        // String TrueOrFlase = remoteMessage.getData().get("AnotherActivity");
        bitmap = getBitmapfromUrl(imageUri);

        sendNotification(message, bitmap, type);
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }


    //This method is only generating push icon_notification
    //It is same as we did in earlier posts
    private void sendNotification(String msg, Bitmap bitmap, String type) {

//        Intent intentNo = new Intent("notificationListener");
//        intentNo.putExtra("notify", true);
//        sendBroadcast(intentNo);


        Intent intent = new Intent(this, MainActivity.class);

        intent.putExtra("type", type);
////        intent.putExtra("isFromNotification", true);
////        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
////        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_item);
////        contentView.setTextViewText(R.id.text_notification_title,messageBody.getTitle());
////        contentView.setTextViewText(R.id.text_notification_subtitle,messageBody.getBody());
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "")
                .setLargeIcon(largeIcon)
                .setSmallIcon(R.drawable.nott)
                .setContentTitle("Edukool Parent")
                .setContentText(msg)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }


    @Override
    public void onCreate() {
        super.onCreate();

    }
}
