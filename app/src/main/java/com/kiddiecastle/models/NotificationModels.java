package com.kiddiecastle.models;

public class NotificationModels{
	private String content;
	private String createdDate;
	private String attachment;
	private String iD;

	public void setContent(String content){
		this.content = content;
	}

	public String getContent(){
		return content;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setAttachment(String attachment){
		this.attachment = attachment;
	}

	public String getAttachment(){
		return attachment;
	}

	public void setID(String iD){
		this.iD = iD;
	}

	public String getID(){
		return iD;
	}

	@Override
 	public String toString(){
		return 
			"NotificationModels{" + 
			"content = '" + content + '\'' + 
			",createdDate = '" + createdDate + '\'' + 
			",attachment = '" + attachment + '\'' + 
			",iD = '" + iD + '\'' + 
			"}";
		}
}
