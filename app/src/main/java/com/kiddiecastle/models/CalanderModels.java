package com.kiddiecastle.models;

/**
 * Created by harini on 6/22/2018.
 */

public class CalanderModels {
    private String date, present;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }
}
