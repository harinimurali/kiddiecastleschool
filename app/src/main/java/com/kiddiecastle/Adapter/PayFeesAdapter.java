package com.kiddiecastle.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.kiddiecastle.R;
import com.kiddiecastle.Utils.FontTextViewMedium;
import com.kiddiecastle.Utils.FontTextViewSemibold;
import com.kiddiecastle.models.PayFeesList;

import java.util.List;


/**
 * Created by harini on 6/26/2018.
 */

public class PayFeesAdapter extends RecyclerView.Adapter<PayFeesAdapter.MyViewHolder> {

    private List<PayFeesList> moviesList;
    //  private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(PayFeesList item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextViewSemibold mpay1;
        public FontTextViewMedium mpay2;
        public FontTextViewSemibold mpay3;
        public LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);
            mpay1 = (FontTextViewSemibold) view.findViewById(R.id.paytxt1);
            mpay2 = (FontTextViewMedium) view.findViewById(R.id.paytxt2);
            mpay3 = (FontTextViewSemibold) view.findViewById(R.id.paytxt3);
            linearLayout = (LinearLayout) view.findViewById(R.id.linearlayout);

        }
    }


    public PayFeesAdapter(List<PayFeesList> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_pay_fees_item_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        PayFeesList movie = moviesList.get(position);
        holder.mpay1.setText(movie.getPay1());
        holder.mpay2.setText(movie.getPay2());
        holder.mpay3.setText(movie.getPay3());

       /* holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
        // return 3;
    }
}