package com.kiddiecastle.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kiddiecastle.R;
import com.kiddiecastle.Utils.AddTouchListen;
import com.kiddiecastle.Utils.FontTextViewMedium;
import com.kiddiecastle.Utils.FontTextViewRegular;
import com.kiddiecastle.Utils.FontTextViewSemibold;
import com.kiddiecastle.models.CircularsModels;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



/**
 * Created by harini on 6/22/2018.
 */

public class CircularsAdapter extends RecyclerView.Adapter<CircularsAdapter.MyViewHolder> {
    List<CircularsModels> circularsModels;
    AddTouchListen addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextViewSemibold content;
        public FontTextViewMedium date;
        public FontTextViewRegular attachment;
        public LinearLayout attach_layout;

        public MyViewHolder(View view) {
            super(view);
            content = (FontTextViewSemibold) view.findViewById(R.id.assignsubject);
            date = (FontTextViewMedium) view.findViewById(R.id.assignclasses);
            attachment = (FontTextViewRegular) view.findViewById(R.id.attachment);
            attach_layout = (LinearLayout) view.findViewById(R.id.attach_layout);

        }
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public CircularsAdapter(List<CircularsModels> assignlist) {
        this.circularsModels = assignlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_assignment_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        CircularsModels movie = circularsModels.get(position);

        if(movie.getAttachment().equals("")){
            holder.attach_layout.setVisibility(View.GONE);
        }else{
            holder.attach_layout.setVisibility(View.VISIBLE);
        }
        holder.content.setText(movie.getContent());


        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MMM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(movie.getDate());
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.date.setText(str);

        holder.attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return circularsModels.size();
    }
}
