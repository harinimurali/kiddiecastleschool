package com.kiddiecastle.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.kiddiecastle.R;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by keerthana on 6/7/2018.
 */

public class GridViewadapter extends BaseAdapter {

    private  String[] gridstring;
    private Context mContext;

    private final int[] gridViewImageId;
    AddTouchListen addTouchListen;
    String gd[];


    public interface AddTouchListen {
        //public void onIncrement(int position, TextView integer, TextView displayInteger);
      //  public void onDecrement(int position, TextView integer, TextView displayInteger);
        public void onRemoveClick(int position);
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }


    public GridViewadapter(Context context, String[] gridstring, int[] gridViewImageId) {
        mContext = context;
        this.gridViewImageId = gridViewImageId;
        this.gd = gridstring;
    }

    @Override
    public int getCount() {
        return gd.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int i, View convertView, ViewGroup parent) {
        View gridViewAndroid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            gridViewAndroid = new View(mContext);
            gridViewAndroid = inflater.inflate(R.layout.grid_view_item, null);
            TextView textViewAndroid = (TextView) gridViewAndroid.findViewById(R.id.grid_view_text);
            CircleImageView imageViewAndroid = (CircleImageView) gridViewAndroid.findViewById(R.id.grid_view_image);
            LinearLayout layout = (LinearLayout) gridViewAndroid.findViewById(R.id.layout);
            textViewAndroid.setText(gd[i]);
            imageViewAndroid.setImageResource(gridViewImageId[i]);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (addTouchListen != null) {
                        addTouchListen.onRemoveClick(i);
                    }
                }
            });


        } else {
            gridViewAndroid = (View) convertView;
        }

        return gridViewAndroid;
    }


}























