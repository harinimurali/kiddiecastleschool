package com.kiddiecastle.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.kiddiecastle.R;
import com.kiddiecastle.models.ResultModel;

import java.util.List;

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.MyViewHolder> {


    private List<ResultModel> assignlist;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView subject, classname, submissiontype,grade,date;
//        public FontTextViewMedium date;
//        public FontTextViewSemibold name;
//        public FontTextViewLight classname;

        public MyViewHolder(View view) {
            super(view);
            subject =view.findViewById(R.id.subject);
            classname =  view.findViewById(R.id.classname);
            grade=  view.findViewById(R.id.grade);
           // submissiontype = (FontTextViewRegular) view.findViewById(R.id.submisstiontype);
            date = view.findViewById(R.id.date);
            //name = (FontTextViewSemibold) view.findViewById(R.id.name);
           // classname = (FontTextViewLight) view.findViewById(R.id.classes);

        }
    }


    public ResultAdapter(List<ResultModel> assignlist, Context context) {
        this.assignlist = assignlist;
        this.context=context;
    }

    @Override
    public ResultAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_result, parent, false);

        return new ResultAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ResultAdapter.MyViewHolder holder, int position) {
        ResultModel movie = assignlist.get(position);
        holder.subject.setText(movie.getSubject());
        holder.grade.setText(movie.getGrade());
      //  holder.desrp.setText(movie.getDescription());
        holder.classname.setText(movie.getClassname()+"-"+movie.getSection());
      //  holder.classname.setText(movie.getClassname());
        holder.date.setText(movie.getDate() );
     //   holder.submissiontype.setText(movie.getSubmissionType());

    }

    @Override
    public int getItemCount() {
        return assignlist.size();
    }
}