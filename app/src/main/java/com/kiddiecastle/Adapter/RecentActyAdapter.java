package com.kiddiecastle.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.kiddiecastle.R;
import com.kiddiecastle.Utils.FontTextViewLight;
import com.kiddiecastle.Utils.FontTextViewMedium;
import com.kiddiecastle.Utils.FontTextViewRegular;
import com.kiddiecastle.models.RecentActivititesModels;

import java.util.List;

/**
 * Created by keerthana on 6/14/2018.
 */


public class RecentActyAdapter extends RecyclerView.Adapter<RecentActyAdapter.MyViewHolder> {

    private List<RecentActivititesModels> moviesList;
    //  private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(RecentActivititesModels item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextViewMedium mSubject;
        public FontTextViewRegular mClasses;
        public FontTextViewLight mTime, mDate;
        public LinearLayout linearLayout;

        public MyViewHolder(View view) {
            super(view);
            mSubject = (FontTextViewMedium) view.findViewById(R.id.subject);
            mClasses = (FontTextViewRegular) view.findViewById(R.id.classes);
            mDate = (FontTextViewLight) view.findViewById(R.id.date);
            mTime = (FontTextViewLight) view.findViewById(R.id.time);
            linearLayout = (LinearLayout) view.findViewById(R.id.linearlayout);

        }
    }


    public RecentActyAdapter(List<RecentActivititesModels> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recent_vew_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RecentActivititesModels movie = moviesList.get(position);
        holder.mSubject.setText(movie.getActivity());
        holder.mClasses.setText(movie.getSubject());
        holder.mDate.setText(movie.getDate());
        holder.mTime.setText(movie.getClassname());

       /* holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
        // return 3;
    }
}