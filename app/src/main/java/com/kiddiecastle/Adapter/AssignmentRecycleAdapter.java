package com.kiddiecastle.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.kiddiecastle.Fragment.Assignments;
import com.kiddiecastle.R;
import com.kiddiecastle.Utils.FontTextViewLight;
import com.kiddiecastle.Utils.FontTextViewMedium;
import com.kiddiecastle.Utils.FontTextViewRegular;
import com.kiddiecastle.Utils.FontTextViewSemibold;
import com.kiddiecastle.models.Assignment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class AssignmentRecycleAdapter extends RecyclerView.Adapter<AssignmentRecycleAdapter.MyViewHolder> {


    private List<Assignment> assignlist;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public FontTextViewRegular subject,desrp,submissiontype;
        public FontTextViewMedium date;
        public FontTextViewSemibold name;
        public FontTextViewLight classname;

        public MyViewHolder(View view) {
            super(view);
            subject = (FontTextViewRegular) view.findViewById(R.id.subject);
            desrp = (FontTextViewRegular) view.findViewById(R.id.descrp);
            submissiontype = (FontTextViewRegular) view.findViewById(R.id.submisstiontype);
            date = (FontTextViewMedium) view.findViewById(R.id.date);
            name = (FontTextViewSemibold) view.findViewById(R.id.name);
            classname = (FontTextViewLight) view.findViewById(R.id.classes);

        }
    }


    public AssignmentRecycleAdapter(List<Assignment> assignlist) {
        this.assignlist = assignlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.assignment_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Assignment movie = assignlist.get(position);
        holder.subject.setText(movie.getSubject());
        holder.desrp.setText(movie.getDescription());
        holder.name.setText(movie.getName());
        holder.classname.setText(movie.getClassname());
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        Date date = null;
        Date date1 = null;
        String str = null;
        String str1 = null;

        try {
            date = inputFormat.parse(movie.getStartDate());
            date1 = inputFormat.parse(movie.getEndDate());
            str = outputFormat.format(date);
            str1 = outputFormat.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.date.setText(str+" to "+str1);
      //  holder.date.setText(movie.getStartDate()+" - "+movie.getEndDate());
        holder.submissiontype.setText(movie.getSubmissionType());

    }

    @Override
    public int getItemCount() {
        return assignlist.size();
    }
}
