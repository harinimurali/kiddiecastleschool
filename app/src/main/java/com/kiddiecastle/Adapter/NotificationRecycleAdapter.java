package com.kiddiecastle.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.kiddiecastle.R;
import com.kiddiecastle.Utils.FontTextViewLight;
import com.kiddiecastle.Utils.FontTextViewMedium;
import com.kiddiecastle.models.NotificationModels;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by keerthana on 6/18/2018.
 */

public class NotificationRecycleAdapter extends RecyclerView.Adapter<NotificationRecycleAdapter.MyViewHolder> {


    private Context mCtx;
    private List<NotificationModels> notificationList;

    public NotificationRecycleAdapter(Context mCtx, List<NotificationModels> notificationList) {
        this.mCtx = mCtx;
        this.notificationList = notificationList;
    }


    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.noti_recycle_view_item, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //getting the product of the specified position
        NotificationModels movie = notificationList.get(position);

        //binding the data with the viewholder views
        holder.Title.setText(movie.getContent());

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm a");

        try {
            Date date2=formatter.parse(movie.getCreatedDate());
            String a= dateformat.format(date2);
            String b= timeformat.format(date2);
            Log.e("date2",""+a);
            holder.date.setText(a);
            holder.time.setText(b);
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        FontTextViewMedium Title;
        FontTextViewLight date,time;
        ImageView document;


        public MyViewHolder(View itemView) {
            super(itemView);

            Title = (FontTextViewMedium) itemView.findViewById(R.id.subject);
            date = (FontTextViewLight) itemView.findViewById(R.id.date);
            time = (FontTextViewLight) itemView.findViewById(R.id.time);
            document = (ImageView) itemView.findViewById(R.id.document);

        }
    }
}

