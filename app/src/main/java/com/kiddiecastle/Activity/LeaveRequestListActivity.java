package com.kiddiecastle.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.kiddiecastle.Adapter.CustomSpinnerAdapter;
import com.kiddiecastle.Adapter.LeaveAdapter;
import com.kiddiecastle.Adapter.NotificationRecycleAdapter;
import com.kiddiecastle.Adapter.PollsAdapter;
import com.kiddiecastle.Adapter.StudentListAdapter;
import com.kiddiecastle.Helper.LanguageHelper;
import com.kiddiecastle.Helper.Permission;
import com.kiddiecastle.Helper.WS_CallService;
import com.kiddiecastle.Helper.WebserviceUrl;
import com.kiddiecastle.R;
import com.kiddiecastle.Utils.AddTouchListen;
import com.kiddiecastle.Utils.AppPreferences;
import com.kiddiecastle.Utils.FontTextViewMedium;
import com.kiddiecastle.Utils.FontTextViewSemibold;
import com.kiddiecastle.models.ChildrenProfile;
import com.kiddiecastle.models.LeaveRequestList;
import com.kiddiecastle.models.NotificationModels;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by harini on 7/21/2018.
 */

public class LeaveRequestListActivity extends AppCompatActivity {

    List<ChildrenProfile> childrenProfiles;
    ProgressBar progressBar;
    WS_CallService service_Login;
    String ParentNumber;
    String studentid;
    private Spinner spinner;
    private RecyclerView leaverecycle;
    private LeaveAdapter mAdapter;
    ImageView back;
    private List<LeaveRequestList> leaveList;
    FontTextViewMedium norecords;
    String schoolid, versioncode;
    int positions;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    ImageView drpDown;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaverequest_activity);

        spinner = (Spinner) findViewById(R.id.spinner);
        back = (ImageView) findViewById(R.id.back);
        progressBar = (ProgressBar) findViewById(R.id.progressIndicator);
        norecords = (FontTextViewMedium) findViewById(R.id.norecords);
        leaverecycle = findViewById(R.id.recyclecircular);
        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(this);

        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(this,
                R.layout.spinner_layout, childrenProfiles);

        ParentNumber = AppPreferences.getParentNumber(LeaveRequestListActivity.this);
        schoolid = AppPreferences.getSchool_Name(LeaveRequestListActivity.this);
        versioncode = AppPreferences.getSchool_Name(LeaveRequestListActivity.this);
       /* ArrayAdapter<ChildrenProfile> LTRadapter = new ArrayAdapter<ChildrenProfile>(getActivity(), android.R.layout.simple_spinner_item, childrenProfiles);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
        spinner.setAdapter(adapter);
        stuname = findViewById(R.id.student_name);
        stuclass = findViewById(R.id.student_class);
        stuimage = findViewById(R.id.student_image);
        drpDown = findViewById(R.id.drpDown);


        if (childrenProfiles.size() == 1) {
            drpDown.setVisibility(View.GONE);
        } else {
            drpDown.setVisibility(View.VISIBLE);
        }


        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(this)).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(this)).getClasses());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(this)).getProfileImage()).placeholder(R.drawable.student_default).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(this)).getID();

        if (Permission.checknetwork(LeaveRequestListActivity.this)) {
            callAPI();
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Log.e("id", "setof child" + childrenProfiles.get(pos).getID());
                studentid = childrenProfiles.get(pos).getID();
                if (Permission.checknetwork(LeaveRequestListActivity.this)) {
                    callAPI();
                }

            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        drpDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
            }
        });
    }

    private void buttonClick() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        final RecyclerView recyclerView = alertLayout.findViewById(R.id.recycle);
        final ImageView close = alertLayout.findViewById(R.id.close);

        AlertDialog.Builder alert = new AlertDialog.Builder(LeaveRequestListActivity.this);
        //  alert.setTitle(getActivity().getResources().getString(R.string.select_child));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        StudentListAdapter studentListAdapter = new StudentListAdapter(childrenProfiles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LeaveRequestListActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        studentListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.e("position", "-" + childrenProfiles.get(position).getName());
                AppPreferences.setStudentPosition(getApplicationContext(), position);
                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(LeaveRequestListActivity.this)).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(LeaveRequestListActivity.this)).getClasses());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(LeaveRequestListActivity.this)).getProfileImage()).placeholder(R.drawable.student_default).into(stuimage);
                studentid = childrenProfiles.get(AppPreferences.getSudentPosition(LeaveRequestListActivity.this)).getID();
                callAPI();
                dialog.dismiss();
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void callAPI() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            leaverecycle.setVisibility(View.GONE);
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            byte[] data;
            String login_str = "UserName:" + schoolid + "|student_id:" + studentid + "|Function:studentLeaveList|Device Type:Android|GCMKey:|DeviceID:'" + Build.ID + "'|AppID:1|IMEINumber:|AppVersion:1.1|MACAddress:|OSVersion:'" + Build.VERSION.SDK + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_Leave_WS load_plan_list = new Load_Leave_WS(this, login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class Load_Leave_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        Context context_aact;

        public Load_Leave_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressBar.setVisibility(View.GONE);
            leaveList = new ArrayList<>();
            leaveList.clear();
            Log.e("jsonResponse", "notificationlist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {
                    /*if (jObj.getString("StatusCode").equals("201")) {
                        norecords.setVisibility(View.VISIBLE);
                    } else {*/
                    norecords.setVisibility(View.GONE);
                    leaverecycle.setVisibility(View.VISIBLE);
                    JSONArray array = jObj.getJSONArray("list");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        LeaveRequestList models = new LeaveRequestList();
                        models.setId(jsonObject.getString("Id"));
                        models.setStaffId(jsonObject.getString("staff_id"));
                        models.setStudentId(jsonObject.getString("student_id"));
                        models.setMessage(jsonObject.getString("message").trim());
                        models.setSubject(jsonObject.getString("subject").trim());
                        models.setFromDate(jsonObject.getString("fromDate"));
                        models.setToDate(jsonObject.getString("toDate"));
                        models.setApprovedStatus(jsonObject.getString("approvedStatus").trim());
                        models.setTeacherRemarks(jsonObject.getString("teacherRemarks").trim());
                        leaveList.add(models);
                    }
                    mAdapter = new LeaveAdapter(LeaveRequestListActivity.this, leaveList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    leaverecycle.setLayoutManager(mLayoutManager);
                    leaverecycle.setItemAnimator(new DefaultItemAnimator());
                    leaverecycle.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();

                    mAdapter.setOnClickListen(new LeaveAdapter.AddTouchListen() {

                        @Override
                        public void onTouchClick(int position) {
                            String id = leaveList.get(position).getId();
                            positions = position;
                            callAPIDelete(id);

                        }

                    });
                    //  }
                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    leaveList.clear();
                    leaverecycle.setVisibility(View.GONE);
                    //  mAdapter.notifyDataSetChanged();
                    String Message = jObj.getString("response");
                    norecords.setVisibility(View.VISIBLE);

                    // Toast.makeText(LeaveRequestListActivity.this, Message, Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    private void callAPIDelete(String leavid) {
        try {
            leaverecycle.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            byte[] data;
            String login_str = "UserName:" + schoolid + "leave_id:" + leavid + "|Function:studentLeaveDelete|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            //String login_str = "student_id:" + studentid + "|Function:studentLeaveList|Device Type:Android|GCMKey:|DeviceID:'" + Build.ID + "'|AppID:1|IMEINumber:|AppVersion:1.1|MACAddress:|OSVersion:'" + Build.VERSION.SDK + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_LeaveDelete_WS load_plan_list = new Load_LeaveDelete_WS(this, login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class Load_LeaveDelete_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        Context context_aact;

        public Load_LeaveDelete_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            leaverecycle.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            Log.e("jsonResponse", "notificationlist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {
                    /*if (jObj.getString("StatusCode").equals("201")) {
                        norecords.setVisibility(View.VISIBLE);
                    } else {*/
                    norecords.setVisibility(View.GONE);
                    leaveList.remove(positions);
                    mAdapter.notifyDataSetChanged();

                    //  }
                    System.out.println("success");
                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("response");
                    norecords.setVisibility(View.VISIBLE);

                    //  Toast.makeText(LeaveRequestListActivity.this, Message, Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }

}
