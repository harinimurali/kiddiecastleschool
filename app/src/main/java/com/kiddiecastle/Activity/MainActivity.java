package com.kiddiecastle.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.kiddiecastle.Adapter.DrawerAdapter;
import com.kiddiecastle.Fragment.Assignments;
import com.kiddiecastle.Fragment.CircularsFragment;
import com.kiddiecastle.Fragment.HomePageFragment;
import com.kiddiecastle.Fragment.LanguageFragment;
import com.kiddiecastle.Fragment.LeaveRequestFragment;
import com.kiddiecastle.Fragment.PayFees;
import com.kiddiecastle.Fragment.Polls;
import com.kiddiecastle.Fragment.RecentActivities;
import com.kiddiecastle.Fragment.ResultFragment;
import com.kiddiecastle.Fragment.StudentAttendance;
import com.kiddiecastle.Helper.LanguageHelper;
import com.kiddiecastle.Helper.WS_CallService;
import com.kiddiecastle.Helper.WebserviceUrl;
import com.kiddiecastle.R;
import com.kiddiecastle.Utils.AppPreferences;
import com.kiddiecastle.Utils.Constants;
import com.kiddiecastle.Utils.FontTextViewMedium;
import com.kiddiecastle.Utils.FontTextViewSemibold;
import com.kiddiecastle.models.ChildrenProfile;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private DrawerLayout drawer;
    ListView listView;
    GridView gridview;
    ImageView notification;
    SharedPreferences sharedPreferences;
    private ActionBarDrawerToggle drawerToggle;
    public FrameLayout fragment_container;
    public static FontTextViewSemibold title;
    FontTextViewSemibold parentname;
    FontTextViewMedium phonenumber;
    Toolbar toolbar;
    String sidemenu[];
    String numb, schoolid,fcm_key;
    WS_CallService service_Login;
    boolean doubleBackToExitPressedOnce = false;
//    String[] gridViewString = {
//            "ATTENDANCE", "RECENT ACTIVITIES",
//            "ASSIGNMENTS", "RESULTS",
//            "VIDEOS", "PAY FEES",
//
//    } ;
//    int[] gridViewImageId = {
//            R.drawable.att,  R.drawable.att,
//            R.drawable.att, R.drawable.att,
//            R.drawable.att, R.drawable.att
//
//    };
    // private ListView mDrawerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_drawer);
        sidemenu = getResources().getStringArray(R.array.system);
        sharedPreferences = getSharedPreferences(Constants.LANGUAGE, MODE_PRIVATE);
        Log.e("shared pref ", "value" + sharedPreferences.getInt(Constants.LANGUAGE_CHANGE, 0));
        drawer = findViewById(R.id.drawer_layout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        gridview = (GridView) findViewById(R.id.grid_view);
        listView = (ListView) findViewById(R.id.navdrawer);
        title = (FontTextViewSemibold) findViewById(R.id.title);
        parentname = (FontTextViewSemibold) findViewById(R.id.username);
        phonenumber = (FontTextViewMedium) findViewById(R.id.mobilenumber);
        notification = (ImageView) findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NotificationActivity.class));
            }
        });
        String fathername = AppPreferences.getParentName(MainActivity.this);
        String phone = AppPreferences.getParentNumber(MainActivity.this);
        Log.e("set", "" + fathername);
        parentname.setText(fathername);
        phonenumber.setText(phone);
        numb = AppPreferences.getParentNumber(getApplicationContext());
        schoolid = AppPreferences.getSchool_Name(getApplicationContext());
        fcm_key = AppPreferences.getFcmToken(getApplicationContext());

        Fragment fragment = new HomePageFragment();
        replaceFragment(fragment);
        try {
            Intent i = getIntent();
            if (i.hasExtra("type")) {
                String pushtype = i.getStringExtra("type");
                if (pushtype.equals("Circulars")) {
                    Fragment fragmentc = new CircularsFragment();
                    replaceFragment(fragmentc);
                    title.setText(getResources().getString(R.string.circulars));
                } else if (pushtype.equals("Assignment")) {
                    Fragment fragmenta = new Assignments();
                    replaceFragment(fragmenta);
                    title.setText(getResources().getString(R.string.assignments));
                } else if (pushtype.equals("Events")) {
                    Fragment fragmente = new Polls();
                    replaceFragment(fragmente);
                    title.setText(getResources().getString(R.string.polls));
                } else if (pushtype.equals("StudentActivity")) {
                    Fragment fragmente = new RecentActivities();
                    replaceFragment(fragmente);
                    title.setText(getResources().getString(R.string.recentactivities));
                } else if (pushtype.equals("payment")) {
                    Fragment fragmentb = new PayFees();
                    replaceFragment(fragmentb);
                    title.setText(getResources().getString(R.string.payfees));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                android.app.FragmentManager fragmentManager = getFragmentManager();
                switch (position) {
                    case 0:
                        drawer.closeDrawers();
                        Fragment fragment = new HomePageFragment();
                        replaceFragment(fragment);
                        //setTitle("Edukool Parent");
                        title.setText(getResources().getString(R.string.app_name));

                        break;

                    case 1:
                        drawer.closeDrawers();
                        Fragment fragment1 = new StudentAttendance();
                        replaceFragment(fragment1);
                        //setTitle("Attendance");
                        title.setText(getResources().getString(R.string.attendance));

                        break;
                    case 2:
                        drawer.closeDrawers();
                        Fragment fragmentt = new RecentActivities();
                        replaceFragment(fragmentt);
                        // setTitle("Recent Activities");
                        title.setText(getResources().getString(R.string.recentactivities));
                        break;
                    case 3:
                        drawer.closeDrawers();
                        Fragment fragment2 = new Assignments();
                        replaceFragment(fragment2);
                        // setTitle("Assignment");
                        title.setText(getResources().getString(R.string.assignments));
                        break;
                    case 4:
                        drawer.closeDrawers();
                        Fragment result = new ResultFragment();
                        replaceFragment(result);
                        //toolbar.setTitle("Results");
                        title.setText(getResources().getString(R.string.results));
                        break;

                    case 5:
                        drawer.closeDrawers();
                        Fragment circle = new CircularsFragment();
                        replaceFragment(circle);
                        // toolbar.setTitle("Circulars");
                        title.setText(getResources().getString(R.string.circulars));
                        break;

                    case 6:
                        drawer.closeDrawers();
                        Fragment leave = new LeaveRequestFragment();
                        replaceFragment(leave);
                        // setTitle("Pay Fees");
                        title.setText(getResources().getString(R.string.leaverequest));
                        break;
                    case 7:
                        drawer.closeDrawers();
                        Fragment pol = new Polls();
                        replaceFragment(pol);
                        // setTitle("Pay Fees");
                        title.setText(getResources().getString(R.string.polls));
                        break;


                    case 8:
                        drawer.closeDrawers();
                        Fragment fee = new PayFees();
                        replaceFragment(fee);
                        // setTitle("Pay Fees");
                        title.setText(getResources().getString(R.string.payfees));
                        break;

                    case 9:
                        drawer.closeDrawers();
                        Fragment lang = new LanguageFragment();
                        replaceFragment(lang);
                        title.setText(getResources().getString(R.string.language));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 10:
                        drawer.closeDrawers();
                        logoutDialog();
                        // title.setText("Language");
                        break;

                }
            }
        });

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.info_icon);
            toolbar.setTitleTextColor(getResources().getColor(R.color.abc_primary_text_material_dark));
            toolbar.setTitle("Edukool Parent");
        }

        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(drawerToggle);

        //  mDrawerList = (ListView) findViewById(R.id.navdrawer);
        DrawerAdapter adapter1 = new DrawerAdapter(MainActivity.this, sidemenu);
        listView.setAdapter(adapter1);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
//        GridViewadapter adapterViewAndroid = new GridViewadapter(MainActivity.this, gridViewString, gridViewImageId);
//        gridview=(GridView)findViewById(R.id.grid_view);
//
//        gridview.setAdapter(adapterViewAndroid);
//        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int i, long id) {
//             //   Toast.makeText(GridViewImageTextActivity.this, "GridView Item: " + gridViewString[+i], Toast.LENGTH_LONG).show();
//            }
//        });
    }

    public void setTitleText(String text) {
        title.setText(text);
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

   /* @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }*/

    @Override
    public void onBackPressed() {
        Log.e("back pressed", "back pressed");

        FragmentManager manager = getSupportFragmentManager();

        Log.e("manager", "entrycount" + manager.getBackStackEntryCount());

        if (manager.getBackStackEntryCount() == 1) {

           /* // Toast.makeText(MainActivity.this, "skjfghgsdfhjdsf", Toast.LENGTH_SHORT).show();
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            } else {
                Toast.makeText(getBaseContext(), "Tap back button in order to exit", Toast.LENGTH_SHORT).show();
            }*/

            //  mBackPressed = System.currentTimeMillis();
            String backStateName;
            backStateName = ((Object) new HomePageFragment()).getClass().getName();
            String fragmentTag = backStateName;

            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main, new HomePageFragment(), fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();

            if (doubleBackToExitPressedOnce) {
                finish();
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
        super.onBackPressed();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageHelper.onAttach(base, LanguageHelper.getLanguage(base)));
    }

    public void logoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.confrim);
        builder.setMessage(R.string.logout_string)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        CallApi();
                       /* List<ChildrenProfile> ch = new ArrayList<>();
                        ChildrenProfile p = new ChildrenProfile();
                        AppPreferences.setParentNumber(MainActivity.this, "");
                        AppPreferences.setLanguage(MainActivity.this, 0);
                        AppPreferences.setParentName(MainActivity.this, "");
                        AppPreferences.saveLoginData(MainActivity.this, false, "", "", "", "", "", "", "", "");
                        AppPreferences.setchildrenProfile(MainActivity.this, ch);
                        AppPreferences.setParentImgae(MainActivity.this, "");
                        startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));*/

                        // FIRE ZE MISSILES!
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.show();
    }



   /* public String getVersionInstalled() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return null;
    }*/

    private void CallApi() {
        try {
            ArrayList<NameValuePair> logout = new ArrayList<NameValuePair>();

            byte[] data;
            String login_str = "UserName:" + schoolid + "|parentId:" + numb + "|Function:ParentLogout|DeviceType:android|GCMKey:''|fcm_userkey:" + fcm_key + "DeviceID:" + Build.ID + "|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:" + Build.VERSION.SDK + "";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            logout.add(new BasicNameValuePair("WS", base64_register));
            Load_Login_WS load_plan_list = new Load_Login_WS(getApplicationContext(), logout);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class Load_Login_WS extends AsyncTask<String, String, String> {
        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Login_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            //  hideProgressBar();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {

                    List<ChildrenProfile> ch = new ArrayList<>();
                    ChildrenProfile p = new ChildrenProfile();
                    AppPreferences.setParentNumber(MainActivity.this, "");
                    AppPreferences.setLanguage(MainActivity.this, 0);
                    AppPreferences.setParentName(MainActivity.this, "");
                    AppPreferences.saveLoginData(MainActivity.this, false, "", "", "", "", "", "", "", "");
                    AppPreferences.setchildrenProfile(MainActivity.this, ch);
                    AppPreferences.setParentImgae(MainActivity.this, "");
                    startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));

                }
            } catch (Exception e) {

            }
        }
    }
}

