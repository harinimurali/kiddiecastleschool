package com.kiddiecastle.Fragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kiddiecastle.Activity.MainActivity;
import com.kiddiecastle.Activity.VideoActivity;
import com.kiddiecastle.Adapter.CircularHomeAdapter;
import com.kiddiecastle.Adapter.GridViewadapter;
import com.kiddiecastle.Adapter.RecentActyAdapter;
import com.kiddiecastle.Adapter.StudentListAdapter;
import com.kiddiecastle.Helper.Permission;
import com.kiddiecastle.Helper.WS_CallService;
import com.kiddiecastle.Helper.WebserviceUrl;
import com.kiddiecastle.R;
import com.kiddiecastle.Utils.AddTouchListen;
import com.kiddiecastle.Utils.AppPreferences;
import com.kiddiecastle.Utils.FontTextViewSemibold;
import com.kiddiecastle.models.ChildrenProfile;
import com.kiddiecastle.models.CircularsModels;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomePageFragment extends Fragment {
    RecyclerView lv;
    //    static String[] values = {"Attendance", "Recent Activities",
//            "Assignment", "Results", "Videos", "Pay Fees",};
    String gd[];
    GridView gridview;
    private CircularHomeAdapter mAdapter;
    private List<CircularsModels> circularsModels;
    WS_CallService service_Login;
    List<ChildrenProfile> profiles;
    String studentid;
    ProgressBar progressBar;
    RelativeLayout layout;
    String ParentNumber, schoolid, versioncode;
    FontTextViewSemibold stuname, stuclass;
    CircleImageView stuimage;
    List<ChildrenProfile> childrenProfiles;
    ImageView drpDown;


    int[] gridViewImageId = {
            R.drawable.attendance, R.drawable.recent_activities,
            R.drawable.assignments, R.drawable.results,
            R.drawable.videos, R.drawable.payfees,};

    private String[] gridstring;


    public static HomePageFragment newInstance() {
        HomePageFragment homePageFragment = new HomePageFragment();
        return homePageFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitleText(getResources().getString(R.string.app_name));
        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default_icon).into(stuimage);
        studentid = childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getID();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_home_page, null);
        lv = (RecyclerView) rootview.findViewById(R.id.list_view1);
        gridview = (GridView) rootview.findViewById(R.id.grid_view);
        stuname = rootview.findViewById(R.id.student_name);
        stuclass = rootview.findViewById(R.id.student_class);
        stuimage = rootview.findViewById(R.id.student_image);
        drpDown = rootview.findViewById(R.id.drpDown);
        gridstring = getResources().getStringArray(R.array.gridview);
        layout = rootview.findViewById(R.id.layout);
        progressBar = (ProgressBar) rootview.findViewById(R.id.progressIndicator);
        profiles = new ArrayList<>();
        profiles = AppPreferences.getchildrenProfile(getActivity());


        childrenProfiles = new ArrayList<>();
        childrenProfiles = AppPreferences.getchildrenProfile(getActivity());
        GsonBuilder gsonBuilder = new GsonBuilder();

        // This is the main class for using Gson. Gson is typically used by first constructing a Gson instance and then invoking toJson(Object) or fromJson(String, Class) methods on it.
        // Gson instances are Thread-safe so you can reuse them freely across multiple threads.

        Gson gson = new Gson();
        gson.toJson(childrenProfiles);

        if (childrenProfiles.size() == 1) {
            drpDown.setVisibility(View.GONE);
        } else {
            drpDown.setVisibility(View.VISIBLE);
        }

        stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
        stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses());
        Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default_icon).into(stuimage);
        studentid = profiles.get(AppPreferences.getSudentPosition(getActivity())).getID();



       /* for (int i = 0; i < jsonArray.length(); i++) {
            try {

                JSONObject object = jsonArray.getJSONObject(i);
                stuname.setText(object.getString("Name"));
                stuclass.setText(object.getString("Class"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            }*/


        ParentNumber = AppPreferences.getParentNumber(getActivity());
        schoolid = AppPreferences.getSchool_Name(getActivity());
        versioncode = AppPreferences.getversioncode(getActivity());
        GridViewadapter adapter = new GridViewadapter(getActivity(), gridstring, gridViewImageId);

        gridview.setAdapter(adapter);
        /*GradientDrawable gdd = new GradientDrawable(
                GradientDrawable.Orientation.RIGHT_LEFT,
                new int[] {0x9c60d3,0xfd84ff});
        gdd.setCornerRadius(0f);
        layout.setBackgroundDrawable(gdd);*/


       /* if (Permission.checknetwork(getActivity())) {
            callAPI();
        }*/
        gd = getResources().getStringArray(R.array.gridview);
        adapter.setOnClickListen(new GridViewadapter.AddTouchListen() {

            @Override
            public void onRemoveClick(int position) {
                switch (position) {
                    case 0:

                        Fragment fragment = new StudentAttendance();
                        replaceFragment(fragment);
                        MainActivity.title.setText(getResources().getString(R.string.attendance));


                        break;

                    case 1:

                        Fragment fragment1 = new RecentActivities();
                        replaceFragment(fragment1);
                        MainActivity.title.setText(getResources().getString(R.string.recentactivities));


                        break;
                    case 2:
                        Fragment fragment2 = new Assignments();
                        replaceFragment(fragment2);
                        MainActivity.title.setText(getResources().getString(R.string.assignments));


                        break;

                    case 3:
                        Fragment fragment3 = new ResultFragment();
                        replaceFragment(fragment3);
                        MainActivity.title.setText(getResources().getString(R.string.results));


                        break;
                    case 4:
                        Intent i = new Intent(getActivity(), VideoActivity.class);
                        startActivity(i);

                        break;
                    case 5:
                        Fragment fragment4 = new PayFees();
                        replaceFragment(fragment4);
                        MainActivity.title.setText(getResources().getString(R.string.payfees));


                        break;

                }
            }

        });

        // gridview.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, values));
        //  prepareMovieData();

        drpDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClick();
            }
        });
        return rootview;

    }

    private void buttonClick() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alert_dialog, null);
        final RecyclerView recyclerView = alertLayout.findViewById(R.id.recycle);
        final ImageView close = alertLayout.findViewById(R.id.close);

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        //  alert.setTitle(getActivity().getResources().getString(R.string.select_child));
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        final AlertDialog dialog = alert.create();
        dialog.show();

        StudentListAdapter studentListAdapter = new StudentListAdapter(childrenProfiles);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(studentListAdapter);
        studentListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Log.e("position", "-" + childrenProfiles.get(position).getName());
                AppPreferences.setStudentPosition(getActivity(), position);
                stuname.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getName());
                stuclass.setText(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getClasses());
                Picasso.get().load(childrenProfiles.get(AppPreferences.getSudentPosition(getActivity())).getProfileImage()).placeholder(R.drawable.student_default_icon).into(stuimage);
                studentid = profiles.get(AppPreferences.getSudentPosition(getActivity())).getID();

                dialog.dismiss();
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

    private void callAPI() {
        try {
            progressBar.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
            ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
            // String schoolid = "SC-005-LS";
            // studentid = "1";
            byte[] data;
            String login_str = "UserName:" + schoolid + "|parentId:" + ParentNumber + "|studentId:" + studentid + "|Function:ParentCirculars|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:" + versioncode + "|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            //  String login_str = "UserName:'" + schoolid + "'|parentId:'9994922081'|studentid:1|Function:StudentActivity|DeviceType:android|GCMKey:''|DeviceID:'" + Build.ID + "'|AppID:1.0|IMEINumber:''|AppVersion:te|MACAddress:''|OSVersion:'" + Build.VERSION.SDK + "'";
            data = login_str.getBytes("UTF-8");
            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
            Log.e("basecode", "" + base64_register);
            login.add(new BasicNameValuePair("WS", base64_register));
            Load_Circulars_WS load_plan_list = new Load_Circulars_WS(getContext(), login);
            load_plan_list.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class Load_Circulars_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        ArrayList<NameValuePair> loginact = new ArrayList<NameValuePair>();
        //        ProgressDialog dialog;
        Context context_aact;

        public Load_Circulars_WS(Context context_ws, ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub

            this.loginact = loginws;
            this.context_aact = context_ws;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            System.out.println("PRE EXECUTE " + loginact.get(0).getValue());

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub.

            try {
                service_Login = new WS_CallService(context_aact);
                jsonResponseString = service_Login.getJSONObjestString(loginact,
                        WebserviceUrl.BASE_URL);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            System.out.println("POST EXECUTE");
            progressBar.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            circularsModels = new ArrayList<>();
            //  circularsModels = new ArrayList<>();
            Log.e("jsonResponse", "schoollist" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);

                String status = jObj.getString("status");
                if (status.equalsIgnoreCase("Success")) {
                    circularsModels.clear();
                    JSONArray array = jObj.getJSONArray("Circulars");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        CircularsModels recent = new CircularsModels();
                        recent.setId(jsonObject.getString("ID"));
                        recent.setContent(jsonObject.getString("Content").trim());
                        recent.setDate(jsonObject.getString("CreatedDate"));
                        recent.setAttachment(jsonObject.getString("Attachment"));
                        circularsModels.add(recent);
                    }
                    mAdapter = new CircularHomeAdapter(circularsModels);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    lv.setLayoutManager(mLayoutManager);
                    lv.setItemAnimator(new DefaultItemAnimator());
                    lv.setAdapter(mAdapter);

                    mAdapter.setOnClickListen(new CircularHomeAdapter.AddTouchListen() {

                        @Override
                        public void onTouchClick(int position) {
                            Fragment frag = new CircularsFragment();
                            replaceFragment(frag);
                            MainActivity.title.setText(getActivity().getResources().getString(R.string.circulars));
                        }
                    });
                    System.out.println("success");

                } else {
                    System.out.println("failed");
                    String Message = jObj.getString("Message");

//                    login_email_pass_error.setVisibility(View.VISIBLE);
//                    login_email_pass_error.setText(Message);

                    Toast.makeText(getActivity(), Message, Toast.LENGTH_LONG).show();
                }


            } catch (Exception e) {

                System.out.println(e.toString() + "zcx");
            }

        }

    }
  /*  private void prepareMovieData() {


        RecentActivititesModels movie = new RecentActivititesModels("Farewell Party", "2 days party in school ","");
        assignmentLists.add(movie);

        movie = new RecentActivititesModels("Book Fair", "assignments_icon on Lessons","");
        assignmentLists.add(movie);

    }*/
}
